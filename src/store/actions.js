import axios from '../axiosConfig'

export default {
    getAllUsers(store){
        axios.get('https://jsonplaceholder.typicode.com/users')
        .then((response)=>{
            store.commit('setUsers',response.data)
        })
        .catch((error)=>{
            throw new Error(error)
        })
    },
    getUserAlbums(store,payload){
        axios.get(`https://jsonplaceholder.typicode.com/users/${payload}/albums`)
        .then((response)=>{
            store.commit('setCurrentUserData',{ data: response.data, title: 'Albums' })
        })
        .catch((error)=>{
            throw new Error(error)
        })
    },
    getUserTodos(store,payload){
        axios.get(`https://jsonplaceholder.typicode.com/users/${payload}/todos`)
        .then((response)=>{
            store.commit('setCurrentUserData', { data: response.data, title: 'Todos' })
        })
        .catch((error)=>{
            throw new Error(error)
        })
    },
    getUserPosts(store,payload){
        axios.get(`https://jsonplaceholder.typicode.com/users/${payload}/posts`)
        .then((response)=>{
            store.commit('setCurrentUserData',{ data: response.data, title: 'Posts' })
        })
        .catch((error)=>{
            throw new Error(error)
        })
    },
    getUserComments(store,payload){
        axios.get(`https://jsonplaceholder.typicode.com/comments?postId=${payload}`)
        .then((response)=>{
            store.commit('setCurrentUserData',{ data: response.data, title: 'Comments' })
        })
        .catch((error)=>{
            throw new Error(error)
        })
    },
    getUserPhotos(store,payload){
        axios.get(`https://jsonplaceholder.typicode.com/photos?albumId=${payload}`)
        .then((response)=>{
            store.commit('setCurrentUserData',{ data: response.data, title: 'Photos' })
        })
        .catch((error)=>{
            throw new Error(error)
        })
    },
    getUser(store,payload){
        axios.get(`https://jsonplaceholder.typicode.com/users/${payload}`)
            .then(response =>{
                store.commit('setCurrentUser',response.data)
            })
            .catch(error =>{
                throw new Error(error)
            })
    },
    pageReloadHelper(store,payload){
        let url = `https://jsonplaceholder.typicode.com`
        switch(payload.name){
            case 'posts':
            url += `/posts/${payload.id}`
            break;
            case 'albums':
            url += `/albums/${payload.id}`
            break;
        }
        axios.get(url)
        .then((response)=>{
            store.commit('setCurrentUserHelper', response.data)
            store.dispatch('getUser',response.data.userId)
        })
        .catch(error => {
            throw new Error(error)
        })
    }

    
    /* getUserPhotos(store,payload){
        // just kidding
        let temp = []
        axios.get(`https://jsonplaceholder.typicode.com/users/${payload}/albums`)
        .then((response)=>{
           temp = response.data.map((obj)=>{ return obj.id })
           
           temp.forEach((id)=>{
               axios.get(`https://jsonplaceholder.typicode.com/photos?albumId=${id}`)
               .then((response)=>{
                    store.commit('appendData',response.data)
               })
               .catch((error)=>{})
           })
        })
        .catch((error)=>{
            throw new Error(error)
        })
    } */
}