export default {
    setUsers(state,payload){
        state.users = payload
    },
    setCurrentUser(state,payload){
        state.currentUser = payload
    },
    setCurrentUserData(state,payload){
        state.currentUserData = payload.data
        state.currentDataTitle = payload.title
    },
    setCurrentUserHelper(state,payload){
        state.currentUserHelper = payload
    },
    resetCurrentUserData(state){
        state.currentUserData = []
        state.currentDataTitle = ''
    },
    
    /* appendData(state,payload){
        state.currentUserData = state.currentUserData.concat(...payload)
    } */
}