export default {
    getUsers(state){
        return state.users
    },
    getUser(state){
        return id => state.users.filter( item => {
            return item.id === id
        })[0]
    },
    getCurrentUser(state){
        return state.currentUser
    },
    getCurrentUserData(state){
        return state.currentUserData
    },
    getCurrentDataTitle(state){
        return state.currentDataTitle
    },
    getCurrentUserHelper(state){
        return state.currentUserHelper
    }
}