import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Posts from './views/Posts.vue'
import Albums from './views/Albums.vue'
import NotFound from './views/NotFound'

Vue.use(Router)

export default new Router({
  //mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about/user/:id',
      name: 'about',
      component: About,
      props: true
    },
    {
      path: '/posts/post/:id',
      name: 'posts',
      component: Posts,
      props: true
    },
    {
      path: '/albums/album/:id',
      name: 'albums',
      component: Albums,
      props: true
    },
    {
      path: '*',
      name: 'not-found',
      component: NotFound,
    }
  ]
})
