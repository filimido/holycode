export default {
    props: {
        listData: {
            type: Array,
            required: true
        },
        size: {
            type: Number,
            required: false,
            default: 10
        }
    },
    data(){
        return {
            pageNumber: 0,
            pageSize: this.size
        }
    },
    methods: {
        nextPage() {
            this.pageNumber++
        },
        previousPage() {
            this.pageNumber--
        }
    },
    computed: {
        pageCount() {
            let l = this.listData.length,
                s = this.pageSize
            return l === s ? 0 : Math.ceil(l / s)
        },
        paginatedData() {
            let start = this.pageNumber * this.pageSize,
                end = start + this.pageSize
                
            return this.listData.slice(start, end)
        }
    },
    created(){
        this.$on('nextPage',()=>{
            this.nextPage()
        })
        this.$on('previousPage',()=>{
            this.previousPage() 
        })
        this.$on('changeSize',(payload)=>{
            this.pageSize = parseInt(payload)
            this.pageNumber = 0
        })
    }

}