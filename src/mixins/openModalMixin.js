
import bus from '../bus'

export default {
    methods: {
        openModal(event, id) {
            let eventSrc = (event.srcElement && event.srcElement.tagName.includes('A')) || event.target.tagName.includes('A')
            let tag = (event.srcElement && event.srcElement.innerText.toLowerCase()) ||  event.target.innerText.toLowerCase()
            console.log(eventSrc,tag)
            if (eventSrc) {
                bus.$emit('openModal', {
                    text:  tag,
                    id
                })
            }
        }
    }
}