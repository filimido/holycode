import bus from '../bus'

export default {
    created(){
        bus.$on('openModal',(payload)=>{
            switch(payload.text){
              case 'albums':
                this.$store.dispatch('getUserAlbums',payload.id)
                break
              case 'todos':
                this.$store.dispatch('getUserTodos',payload.id)
                break
              case 'posts':
                this.$store.dispatch('getUserPosts',payload.id)
                break
            }
            this.modalOpened = true;
          })
    },
    beforeDestroy(){
        bus.$off('openModal')
    }
}