import axios from 'axios'
import bus from './bus'

axios.interceptors.request.use(function (request) {
    //  before request is sent
    bus.$emit("loadStart")

    return request;

  }, function (error) {
    //  request error
    return Promise.reject(error);
  });

// Add a response interceptor
axios.interceptors.response.use(function (response) {
    // response data
    bus.$emit('loadEnd')
    
    return response;

  }, function (error) {
    // response error
    window.location.replace('/')
    return Promise.reject(error);
  });

  export default axios