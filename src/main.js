import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bulma/css/bulma.css'
import * as VueGoogleMaps from 'vue2-google-maps'


Vue.config.productionTip = false

Vue.use(VueGoogleMaps,{
  load: {
    key: 'AIzaSyB0CUezZY8brHEN92q4vR4mrtRbGSs_SwU'
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
